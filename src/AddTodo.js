import React, { Component } from 'react';
import './AddTodo.css';

class AddTodo extends Component {
  render() {
    return (
     
          <form className="row">
          <div className="col-md-3">
          <input aria-label="new task" type="text"  placeholder="Add new task" className="form-control-input" onChange={this.props.handleTextChange} value={this.props.text} />
          </div>
          <button aria-label="new task added" className="btn additem btn-primary" onClick={this.props.handleAddItem} disabled={!this.props.text}>add task</button>

        </form>
      
    );
  }
}

export default AddTodo;
