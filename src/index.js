import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { FaTrash, FaPlusCircle ,FaTimesCircle} from "react-icons/fa";
import AddTodo from "./AddTodo.js";
class TodoApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [
        {
          id: 1,
          text: "jogging",
          done: false,
          color: "#FFD506"
        },
        {
          id: 2,
          text: "send project file",
          done: false,
          color: "#1ED102"
        },
        {
          id: 3,
          text: "email",
          done: false,
          color: "#D10263"
        }
      ],
      text: "",
      showInput: false
    };
    this.inputFromClose = this.inputFromClose.bind(this)
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleAddItem = this.handleAddItem.bind(this);
    this.showInputForm = this.showInputForm.bind(this);
    this.markItemCompleted = this.markItemCompleted.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
  }
  handleTextChange(event) {
    this.setState({
      text: event.target.value
    });
  }
  showInputForm() {
    this.setState({
      showInput: true
    });
  }
  inputFromClose(){
    this.setState({
      showInput: false
    });
  }
  handleAddItem(event) {
    event.preventDefault();

    var newItem = {
      id: Date.now(),
      text: this.state.text,
      done: false,
      color: "green"
    };

    this.setState(prevState => ({
      items: prevState.items.concat(newItem),
      text: ""
    }));
  }
  markItemCompleted(itemId) {
    var updatedItems = this.state.items.map(item => {
      if (itemId === item.id) item.done = !item.done;

      return item;
    });

    this.setState({
      items: [].concat(updatedItems)
    });
  }
  handleDeleteItem(itemId) {
    var updatedItems = this.state.items.filter(item => {
      return item.id !== itemId;
    });

    this.setState({
      items: [].concat(updatedItems)
    });
  }
  render() {
    return (
      <div className="main">
        <h3 className="title">MY TO DO LIST</h3>
       
        {this.state.showInput ? 
        <div>
          <div className="circleClose">
          <FaTimesCircle aria-label="close-input-form" onClick={this.inputFromClose} className="fa-icon-close"></FaTimesCircle>
          </div>
          <div>
          <AddTodo
            handleAddItem={this.handleAddItem}
            handleTextChange={this.handleTextChange}
            text={this.state.text}
          /></div>
          </div>
         : 
          ""
          
        }
        <div className="row">
          <div className="col-md-3">
          <div className="today-label">Today's task:</div>
            <TodoList
              items={this.state.items}
              onItemCompleted={this.markItemCompleted}
              onDeleteItem={this.handleDeleteItem}
            />
          </div>
        </div>
        {
        !this.state.showInput ? 
        <div className="col-md-3">
          <FaPlusCircle aria-label="input-form-show" className="plusButton" onClick={this.showInputForm} />
        </div>:""
        }
      </div>
    );
  }
}

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.markCompleted = this.markCompleted.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }
  markCompleted(event) {
    this.props.onItemCompleted(this.props.id);
  }
  deleteItem(event) {
    this.props.onDeleteItem(this.props.id);
  }
  render() {
    var itemClass =
      "form-check todoitem " + (this.props.completed ? "done" : "undone");
    return (
      <li
        className={itemClass}
        style={{ borderLeft: "5px solid " + this.props.color }}
      >
        <label className="container form-check-label">
          {this.props.text}
          <input
            aria-label="task done"
            type="checkbox"
            className="form-check-input"
            onChange={this.markCompleted}
          />
          <span className="checkmark" />
          <FaTrash
             aria-label="task delete"
            className="delete-icon"
            aria-label="Delete"
            onClick={this.deleteItem}
          />
        </label>
      </li>
    );
  }
}

class TodoList extends React.Component {
  render() {
    return (
      <ul className="todolist">
        {this.props.items.map(item => (
          <TodoItem
            key={item.id}
            id={item.id}
            text={item.text}
            color={item.color}
            completed={item.done}
            onItemCompleted={this.props.onItemCompleted}
            onDeleteItem={this.props.onDeleteItem}
          />
        ))}
      </ul>
    );
  }
}
ReactDOM.render(<TodoApp />, document.getElementById("root"));
serviceWorker.unregister();
